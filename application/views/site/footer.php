 <!-- FOOTER -->
    <footer id="footer" class="classic_footer">
        <!-- Container -->
        <div class="container footer-body">
            <div class="row clearfix">
                <!-- Column -->
                <div class="col-md-3 col-sm-6 col-xs-12 sm-mb-mobile">
                    <!-- Title -->
                    <h6 class="uppercase white extrabold">Quadra Exclusive Theme</h6>
                    <h6 class="sm-mt bold gray8">ABOUT US</h6>
                    <p class="mini-mt">It is a long established fact that a read page when looking at its layout. </p>
                    <h6 class="xs-mt bold gray8"><i class="fa fa-map-marker mini-mr"></i>OUR ADDRESS</h6>
                    <p class="mini-mt">PO Box 16122 Collins Street West <br class="hidden-xs"> Victoria 8007 Australia</p>
                    <!-- Google Map -->
                    <a href="/assets/https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2624.9916256937586!2d2.2922926156743895!3d48.858370079287475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e2964e34e2d%3A0x8ddca9ee380ef7e0!2sEyfel+Kulesi!5e0!3m2!1str!2s!4v1491905379246" data-iframe="true" class="lightbox underline-hover colored-hover">
                        Find us on Google Map
                    </a>
                    <h6 class="xs-mt bold gray8"><i class="fa fa-envelope mini-mr"></i>CONTACT US</h6>
                    <p class="mini-mt">Pbx: <a href="tel:+0123456790" class="underline-hover colored-hover">+0123456789</a></p>
                    <a href="mailto:goldeyestheme@gmail.com" class="underline-hover colored-hover">example@example.com</a>
                </div>
                <!-- Column -->
                <div class="col-md-3 col-sm-6 col-xs-12 all-block-links sm-mb-mobile">
                    <h6 class="uppercase white extrabold sm-mb">Latest News</h6>
                    <!-- You can edit footer-news.html file in js/ajax folder. Will be changed on all website -->
                    <div data-ajax-load="/assets/js/ajax/footer-news.html"><i class="fa fa-refresh fa-2x fa-spin"></i></div> 
                </div>
                <!-- End Column -->
                <!-- Column -->
                <div class="col-md-3 col-sm-6 col-xs-12 sm-mb-mobile">
                    <h6 class="uppercase white extrabold sm-mb">recent Comments</h6>
                    <!-- Clients Slider one - You can find details in footer-client-comments.html file, #post1 div -->
                    <!-- When you edit ajax file, the details will be changed on all website -->
                    <div data-ajax-load="/assets/js/ajax/footer-client-comments.html #post1" class="ajax-slider"></div>
                    <!-- Clients Slider two -->
                    <div data-ajax-load="/assets/js/ajax/footer-client-comments.html #post2" class="ajax-slider"></div>
                    <!-- Sub Title -->
                    <h6 class="xxs-mt bold gray8">ADD YOUR COMMENT</h6>
                    <p class="mini-mt gray8"><a href="/assets/http://goldeyestheme.com/envato/quadra/wishbox.html" target="_blank" class="mini-mt underline-hover gray6-hover gray7">Drop us your comment!</a> All images are placeholders.</p>
                </div>
                <!-- End Column -->
                <!-- Column -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <!-- Title -->
                    <h5 class="uppercase white extrabold no-pm">SUBSCRIBE US</h5>
                    <!-- Sub Title -->
                    <h6 class="sm-mt bold gray8">GET UPDATED</h6>
                    <p class="mini-mt">The standard chunk of Lorem Ipsum used.</p>
                    <div id="newsletter-form" class="footer-newsletter clearfix xs-mt">
                        <form id="newsletter_form" name="newsletter_form" method="post" action="php/newsletter.php">
                            <input type="email" name="n-email" id="n-email" required placeholder="Add your E-Mail address" class="font-12 radius-lg form-control">
                            <button type="submit" id="n-submit" name="submit" class="btn-lg fullwidth radius-lg bg-colored2 gray4 qdr-hover-6 click-effect bold font-12">SUBSCRIBE</button>
                        </form>
                    </div>
                    <!-- End Form -->
                    <h6 class="xs-mt xxs-mb bold gray8">FOLLOW US</h6>
                    <a href="#" class="icon-xs radius bg-dark facebook white-hover slow1"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="icon-xs radius bg-dark twitter white-hover slow1"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="icon-xs radius bg-dark instagram white-hover slow1"><i class="fa fa-instagram"></i></a>
                    <a href="#" class="icon-xs radius bg-dark pinterest white-hover slow1"><i class="fa fa-pinterest"></i></a>
                </div>
                <!-- End Column -->
            </div>
        </div>
        <!-- End Container -->
        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row clearfix calculate-height t-center-xs">
                    <div class="col-sm-6 col-xs-12 table-im t-left height-auto-mobile t-center-xs">
                        <div class="v-middle">
                            <img src="/assets/images/logos/icon_02_b.png" alt="logo icon" class="logo">
                        </div>
                    </div>
                    <!-- Bottom Note -->
                    <div class="col-sm-6 col-xs-12 table-im t-right height-auto-mobile t-center-xs xxs-mt-mobile">
                        <p class="v-middle">
                            <a href="#" target="_blank" class="gray6-hover underline-hover">Term and Condition</a> | 
                            <a href="#" target="_blank" class="gray6-hover underline-hover">Privacy Policy</a> <br class="hidden-xs">
                            © 2018. Powered By 
                            <a href="/assets/https://themeforest.net/item/quadra-creative-multipurpose-template/21409528" target="_blank" class="colored-hover underline-hover">Elite Themeforest Author</a>.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->












    <!-- Quick Contact Form -->
    <div class="quick-contact-form border-colored">
        <h5 class="uppercase t-center extrabold">Drop us a message</h5>
        <p class="t-center normal">You're in the right place! Just drop us a message. How can we help?</p>
        <!-- Contact Form -->
        <form class="quick_form" name="quick_form" method="post" action="php/quick-contact-form.php">
            <!-- Name -->
            <input type="text" name="qname" id="qname" required placeholder="Name" class="no-mt">
            <!-- Email -->
            <input type="email" name="qemail" id="qemail" required placeholder="E-Mail">
            <!-- Message -->
            <textarea name="qmessage" id="qmessage" required placeholder="Message"></textarea>
            <!-- Send Button -->
            <button type="submit" id="qsubmit" class="bg-colored white qdr-hover-6 extrabold">SEND</button>
            <!-- End Send Button -->
        </form>
        <!-- End Form -->
        <a href="/assets/https://themeforest.net/user/goldeyes#contact" target="_blank" class="inline-block colored-hover uppercase extrabold h6 no-pm qdr-hover-5">Or see contact page</a>
    </div>
    <!-- Contact us button -->
    <a href="#" class="drop-msg click-effect dark-effect"><i class="fa fa-envelope-o"></i></a>
    <!-- Back To Top -->
    <a id="back-to-top" href="#top"><i class="fa fa-angle-up"></i></a>





    <!-- SEARCH FORM FOR NAV -->
    <div class="fs-searchform">
        <form id="fs-searchform" class="v-center container" action="pages-search-results.html" method="get">
            <!-- Input -->
            <input type="search" name="q" id="q" placeholder="Search on website.com" autocomplete="off">
            <!-- Search Button -->
            <button type="submit">
                <i class="fa fa-search"></i>
            </button>
            <div class="recommended font-14 normal">
                <h5 class="rcm-title">Recommend Links;</h5>
                <a href="demo-antares.html">Quadra, Antares version</a>
                <a href="demo-athena.html">Beautiful Athena demo</a>
                <a href="elements-all.html">Awesome Quadra Elements</a>
                <a href="demo-feronia.html">Why i will use the Quadra?</a>
                <a href="demo-sun.html">Checkout the Sun demo</a>
                <a href="index.html">See 600+ templates</a>
            </div>
        </form>
        <div class="form-bg"></div>
    </div>
    <!-- END SEARCH FORM -->

    <!-- Messages for contact form -->
    <div id="error_message" class="clearfix">
        <i class="fa fa-warning"></i>
        <span>Validation error occured. Please enter the fields and submit it again.</span>
    </div>
    <!-- Submit Message -->
    <div id="submit_message" class="clearfix">
        <i class="fa fa-check"></i>
        <span>Thank You ! Your email has been delivered.</span>
    </div>


	<!-- jQuery -->
    <script type="text/javascript" src="/assets/js/jquery.min.js"></script>
    <!-- MAIN SCRIPTS - Classic scripts for all theme -->
    <script type="text/javascript" src="/assets/js/scripts.js?v=2.2"></script>
    <script type="text/javascript" src="/assets/content/rise/js/plugins.js?v=2.1"></script>
    <script type="text/javascript" src="/assets/js/revolutionslider/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="/assets/js/revolutionslider/jquery.themepunch.tools.min.js"></script>



    <script type="text/javascript">
        // init cubeportfolio
        $('#products').cubeportfolio({
            filters: '#filter1, #filter2',
            loadMoreAction: 'click',
            layoutMode: 'grid',
            mediaQueries: [{
                width: 1500,
                cols: 6,
            }, {
                width: 1100,
                cols: 6,
            }, {
                width: 600,
                cols: 2,
            }, {
                width: 480,
                cols: 1,
                options: {
                    caption: '',
                }
            }],
            defaultFilter: '*',
            animationType: 'quicksand',
            gapHorizontal: 10,
            gapVertical: 10,
            gridAdjustment: 'responsive',
            caption: '',
            displayType: 'sequentially',
            displayTypeSpeed: 50,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

            // singlePageInline
            singlePageInlineDelegate: '.cbp-singlePageInline',
            singlePageInlinePosition: 'top',
            singlePageInlineInFocus: true,
            singlePageInlineCallback: function(url, element) {
                // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
                var t = this;

                $.ajax({
                        url: url,
                        type: 'GET',
                        dataType: 'html',
                        timeout: 30000
                    })
                    .done(function(result) {

                        t.updateSinglePageInline(result);

                    })
                    .fail(function() {
                        t.updateSinglePageInline('AJAX Error! Please refresh the page!');
                    });
            },
        });


    </script>



</body>
<!-- Body End -->
</html>