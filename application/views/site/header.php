<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Computer</title>
    <meta name="description" content="Quadra Theme Rise Version" />
    <!--Keywords -->
    <meta name="keywords" content="modern, creative, website, html5, bootstrap responsive, parallax, soft, front-end, designer, coming soon, account, portfolio, photographer, grid, social, modules, design, personal, faq, one page, multi-purpose, branding, studio, agency, templates, css3, carousel, slider, corporate, theme, quadra, demos, blog, shop" />
    <meta name="author" content="GoldEyes" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <!--Favicon -->
    <link rel="icon" type="image/png" href="/assets/images/favicon.png" />
    <!-- Apple Touch Icon -->
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png"/>

    <!-- CSS Files -->
    <link rel="stylesheet" href="/assets/css/plugins.css?v=2.2"/>
    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/assets/css/revolutionslider/settings.css" />
        <link rel="stylesheet" href="/assets/css/revolutionslider/settings.css" />
    <!-- Theme Styles -->
    <link rel="stylesheet" href="/assets/css/theme.css?v=2.2"/>
    <!-- Page Styles -->
    <link rel="stylesheet" href="/assets/content/rise/css/rise.css" />
    
    <!-- End Page Styles -->



</head>

