<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	
	public function index() {
        $this->load->view('site/header');
        $this->load->view('site/main');
        $this->load->view('site/footer');
        
        
    }
}
